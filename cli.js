#!/usr/bin/env node

const config = require('config')
const fs = require('fs')

try {
  if (fs.existsSync('./app.json')) {
    const manifest = JSON.parse(fs.readFileSync('./app.json', 'utf8'))
    if (!manifest.expo.extra) {
      manifest.expo.extra = {}
    }
    manifest.expo.extra.expoConfig = config
    fs.writeFileSync('./app.json', JSON.stringify(manifest, null, 2))
    process.exit(0)
  } else {
    console.error('there is no app.json file to update')
    process.exit(1)
  }
} catch (err) {
  console.error('something went wrong: ' + err.message)
  process.exit(1)
}